package com.emirates.aacs.flightpopup.mw.service.impl;

import java.util.List;
import org.slf4j.Logger;


import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.emirates.aacs.constants.AppModule;
import com.emirates.aacs.constants.ErrorSeverities;
import com.emirates.aacs.constants.ErrorTypes;
import com.emirates.aacs.constants.LogTypes;
import com.emirates.aacs.exception.annotations.AACSSystemLogMethod;
import com.emirates.aacs.flightpopup.mw.service.MWAllocationInfoService;
import com.emirates.aacs.taskmonitoring.constant.TaskMonitoringConstant;
import com.emirates.aacs.taskmonitoring.flightpopup.dto.AllocationInfoTO;
import com.emirates.aacs.taskmonitoring.flightpopup.dto.FlightInfoSearchTO;
import com.emirates.aacs.taskmonitoring.flightpopup.dto.ResourceTO;
import com.emirates.aacs.taskmonitoring.flightpopup.dto.TaskAllocationTO;
import com.emirates.dnata.sgl.common.utils.AppUtils;
import com.emirates.dnata.sgl.dto.requestlog.RequestLog;
import com.emirates.egsframework.core.annotations.EGSService;
import com.emirates.restconfig.service.impl.TaskMonitoringConfigServiceImpl;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

@Service(value = "mwAllocationInfoService")
@EGSService(modulecode = "mwAllocationInfoService")
public class MWAllocationInfoServiceImpl extends TaskMonitoringConfigServiceImpl implements  MWAllocationInfoService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	@AACSSystemLogMethod(errorType = ErrorTypes.SERVICEANDAPPLICATION, 
	severity = ErrorSeverities.CRITICAL_ERROR, 
	source = AppModule.FLIGHTPOPUPSERVICE, 
	errorCode = ErrorSeverities.DEFAULT_ERROR_CODE,
	auditServiceReturnValues = false, 
	auditServiceArgumentValues = false, 
	module = TaskMonitoringConstant.FLIGHTPOPUP, 
	logType = LogTypes.ERROR)
	public List<AllocationInfoTO> getFlightAllocation(RequestLog requestLog, FlightInfoSearchTO searchTO) {
		

		List<AllocationInfoTO> response = null;
		searchTO.setRequestLog(requestLog);
		String url = null;	
	
		if (mwIsLocalEnv) {
			url = AppUtils.getUrl(mwTaskMonitoringLocalUrl + "flightpopup/allocationInfo/getFlightAllocation", null);
		} else {
			url = AppUtils.getUrl(mwTaskMonitoringServerUrl + "flightpopup/allocationInfo/getFlightAllocation", null);
		}
		
		HttpHeaders headers = restClientService.getHttpHeaders();
		
		HttpEntity<FlightInfoSearchTO> requestEntity = new HttpEntity<FlightInfoSearchTO>(searchTO, headers);	
		strResponse = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		restClientService.verifyResponseEntity(strResponse);
		
		ObjectMapper objectMapper = new ObjectMapper(); 
		objectMapper.getTypeFactory();
		
		try {
			response = objectMapper.readValue(strResponse.toString(), TypeFactory.defaultInstance().constructCollectionType(List.class, AllocationInfoTO.class));
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//response= AppUtils.fromJson(strResponse.getBody(), AllocationInfoTO.class);
		return response;
		
	}

	@Override
	public List<AllocationInfoTO> getOutStnFlightAllocation(
			RequestLog requestLog, FlightInfoSearchTO searchTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TaskAllocationTO> getTaskAllocation(RequestLog requestLog,
			FlightInfoSearchTO searchTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ResourceTO> getResourceAllocation(RequestLog requestLog,
			FlightInfoSearchTO searchTO) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
